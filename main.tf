resource "random_id" "instance_id" {
  byte_length = 8
}
resource "google_storage_bucket" "bucket" {
#Number of bucket
 count = 1
#Bucket name
 name = "testbucket-${random_id.instance_id.hex}"
# Any location of your choice
 location = "europe-west2"
# Any storage_class of your choice
 storage_class = "COLDLINE"
}
